import os
import cv2
import numpy as np
from PIL import Image

recognizer = cv2.createLBPHFaceRecognizer()
path='dataSet'

def getImagesWithID(path):
    imagePaths=[os.path.join(path,f) for f in os.listdir(path)] 
    faces=[]
    IDs=[]
    #now looping through all the image paths and loading the Ids and the images
    for imagePath in imagePaths:
        faceImg=Image.open(imagePath).convert('L');
        #Now we are converting the PIL image into numpy array
        faceNp=np.array(faceImg,'uint8')
        #getting the Id from the image
        ID=int(os.path.split(imagePath)[-1].split('.')[1])
        faces.append(faceNp)
        print ID        # extract the face from the training image sample
        
        #If a face is there then append that in the list as well as Id of it
        
        IDs.append(ID)
        cv2.imshow("training",faceNp)
        cv2.waitKey(10)
    return IDs,faces


IDs,faces= getImagesWithID(path)
recognizer.train(faces, np.array(IDs))
recognizer.save('recognizer/trainningdata.yml')
cv2.destroyAllWindows()
